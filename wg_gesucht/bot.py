#!/usr/bin/env python
import telebot
import time
# from datetime import datetime as dt
from utils import wg_scraper as scraper
import requests
import re
import concurrent.futures 

# debug: stops taking user resp after some time; same ad got sent twice

bot = telebot.TeleBot("1807751733:AAEzgetEbJ5bvBSxETpA0jsGVVvxxEljaIc")
base_url = 'https://www.wg-gesucht.de/'
user_dict = {} # contains {chat_id: (url, lastchecked, subscribed)}

keyboard = telebot.types.InlineKeyboardMarkup()
keyboard.add(telebot.types.InlineKeyboardButton('start', callback_data='yes'),
             telebot.types.InlineKeyboardButton('stop', callback_data='no'))

# keyboard buttons instruction
@bot.message_handler(commands=['notifyme'])
def like(message):
  cid = message.chat.id
  if cid in user_dict: 
    bot.send_message(cid, "To start getting new post notifications, press start. To stop notifications, press stop " + u'\U0001F447', reply_markup=keyboard)
  else: 
    bot.send_message(cid, "Please enter the seach URL first " + u'\U0001F605')

# keyboard buttons response - start or stop search
@bot.callback_query_handler(func=lambda call: call.data in ['yes', 'no'])
def callback_hander(call):
    global user_dict 
    chat_id = call.message.chat.id
    if call.data == 'yes':
        if chat_id in user_dict: # ----------- subscribe user -----------------
            if user_dict[chat_id][2]: 
                bot.send_message(chat_id, "You're already subscribed :/")
                return 
            user_dict[chat_id][2] = True 
            bot.send_message(chat_id, "You are now subscribed to updates!")
            
            # debug: maybe shouldn't use threadpool?  what about loop here to check time and send update to user every x minutes? 
            with concurrent.futures.ThreadPoolExecutor() as executor: 
                executor.submit(send_continuous_updates, chat_id)

        else: 
            bot.send_message(chat_id, "Please enter your WG-Gesucht search URL first " + u'\U0001F613')
    else:
        if chat_id in user_dict: # --------------- unsubscribe ---------------------
            user_dict[chat_id][2] = False
            bot.send_message(chat_id, "You will no longer receive notifications. Goodbye " + u'\U0001F44B')
        else: 
            bot.send_message(chat_id, "No notifications to stop, you never signed up for any " + u'\U0001F9D0')


@bot.message_handler(commands=['help'])
def answer(message):
    bot.reply_to(message, "To get started, enter your search query on WG-Gesucht, copy the URL and send it to me. Then say /notifyme, press start, and you'll get notifications on new posts "+ u'\U0001F48C' + " You can press stop to unsubscribe from notifications. Say /clear to delete all your notifications and remove your data. ")

@bot.message_handler(commands=['start'])
def answer(message):
    bot.reply_to(message, "Welcome" + u"\U0001F638" + " I scrape WG-Gesucht to keep you updated on new listings for your search. For more info, say /help.")
    
@bot.message_handler(commands=['clear'])
def answer(message):
    global user_dict 
    chat_id = message.chat.id 
    if chat_id in user_dict: # ---------------- remove user ----------------------
        del user_dict[chat_id] 
        bot.send_message(chat_id, "Your data has been removed. Goodbye " + u'\U0001F44B')
    else: 
        bot.send_message(chat_id, "No notifications to stop, you're not in the database " + u'\U0001F9D0')

@bot.message_handler(func=lambda m: True)
def echo_all(message):
    global url
    greet = '^h.*[iyo]$'
    url_pattern = '^(https://|www.).*'
    if re.match(url_pattern, message.text.lower()):
        if 'https://www.wg-gesucht.de/' in message.text: 
            chat_id = message.chat.id
            # --------------------------- add user ---------------------------------
            user_dict[chat_id] = [message.text, 0, False] # [url, last_checked, subscribed]
            bot.reply_to(message, "Well done, you set up the URL " + u'\U0001F601' + " to get notifications, say /notifyme and press the start button") 
        else:
            bot.reply_to(message, "Please search on WG-Gesucht, then give the full URL.")
    elif re.match(greet,message.text.lower()):
        greet = "Hello " + message.from_user.first_name + "! Nice to meet you " +  u'\U0001F600'
        bot.reply_to(message, greet)
    else:
        bot.reply_to(message, "For more info, say /help. To start getting notifications, enter search URL and say /notifyme (press stop to unsubscribe from notifications). " + u'\U0001F4AB') 


def send_updates(chat_id):
    global user_dict
    url, last_checked, subscribed = user_dict[chat_id]

    if not subscribed:
        return

    adid = cost = title = link = period = [] 
    
    print('checking for user ', chat_id)
    try:
        adid, cost, title, link, period = scraper.parse_search(url)
    except Exception as e: 
        print('Error: ', e)
        time.sleep(5)
    print(len(adid), len(cost), len(title), len(link))

    if  len(adid) > 1 and len(adid) ==len(link): 
        zip_postinfo = sorted(zip(adid, period, cost, title, link))
        # show a couple links for first time user and save last_checked post id 
        if last_checked == 0: 
            bot.send_message(chat_id, "Here's the most recent post " + u'\U0001F48C' + " You'll get notifications for all new posts hereafter.")
            a, p, c, t, l = zip_postinfo[-1]
            bot.send_message(chat_id, p + ", " + c + " " + t + "\n" + base_url + l)
            user_dict[chat_id][1] = int(a) # set last_checked
        if last_checked >= int(zip_postinfo[-1][0]): 
            # no new posts, break out of function
            return
        for item in zip_postinfo: 
            a, c, t, l = item 
            # print('last checked: ', last_checked, ' new: ', str(a), ' bool ', int(a) > last_checked)
            if last_checked and int(a) > last_checked: 
                print("sent to ", chat_id)
                bot.send_message(chat_id ,"I found something " + u'\U0001F929' + "\n" + c + " " + t + "\n" + base_url + l)
                user_dict[chat_id][1] = int(a) # update last_checked

def send_continuous_updates(chat_id):
    global user_dict 
    while user_dict[chat_id][2]: 
        send_updates(chat_id) 
        time.sleep(30)

# todo add multiple notifcation urls and option which ones to remove 

bot.polling(none_stop=False, interval=1, timeout=5)
