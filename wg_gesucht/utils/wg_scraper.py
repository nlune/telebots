from lxml import html
import requests

def parse_search(url):
    headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36'} 

    page = requests.get(url, headers=headers)
    tree = html.fromstring(page.content)

    title = tree.xpath('//*[@class="wgg_card offer_list_item  "]//h3/@title')


    cost  = tree.xpath('//*[@class="wgg_card offer_list_item  "]//div[@class="col-xs-3"]/b/text()')

    adid = tree.xpath('//*[@class="wgg_card offer_list_item  "]/@data-id')

    period = [' '.join(i.split()) for i in tree.xpath('//*[@class="wgg_card offer_list_item  "]//div[@class="col-xs-5 text-center"]/text()')]

    date = [' '.join(i.split()) for i in tree.xpath('//*[@class="wgg_card offer_list_item  "]//div[@class="col-xs-5 text-center"]/text()')]

    link  = tree.xpath('//*[@class="wgg_card offer_list_item  "]//h3/a/@href')

    return adid, cost, title, link, period





if __name__ == "__main__": 

    parse_search('https://www.wg-gesucht.de/1-zimmer-wohnungen-in-Berlin.8.1.1.0.html')
