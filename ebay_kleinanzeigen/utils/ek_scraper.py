from lxml import html
import requests

def parse_search(url):
    headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36'} 

    page = requests.get(url, headers=headers)
    tree = html.fromstring(page.content)

    adid = tree.xpath('//article[@class="aditem"]/@data-adid')
    cost = tree.xpath('//p[@class="aditem-main--middle--price"]/text()')
    date = [i.strip() for i in tree.xpath('//div[@class="aditem-main--top--right"]/text()') if i.strip()]
    link = tree.xpath('//a[@class="ellipsis"]/@href')
    title = tree.xpath('//a[@class="ellipsis"]/text()')
    
    cost = [c.strip() for c in cost] 
    adid = [int(i) for i in adid]

    diff = len(adid) - len(date)
    # pad date vector 
    for i in range(diff): 
        date.insert(i, 'top')

    if len(cost) != len(adid):
        cost = ['' for c in range(len(adid))]

    return adid, cost, title, link, date

