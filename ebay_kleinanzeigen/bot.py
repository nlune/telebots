#!/usr/bin/env python
import telebot
import time
from datetime import datetime as dt
from utils import ek_scraper as scraper
import requests
import re
import concurrent.futures 
# import multithreading
# todo: stops working with multiple threads (doesn't receive user input)

TOKEN = "1616023779:AAH-J94kLVTvChQC6ArfMy1fyPO3yD1mhy0"
bot = telebot.AsyncTeleBot(TOKEN)
base_url = 'https://www.ebay-kleinanzeigen.de' 
user_dict = {} # contains {chat_id: (url, lastchecked, subscribed)}
# bot.send_message(470027057, "say commands /help if you're confused")

keyboard = telebot.types.InlineKeyboardMarkup()
keyboard.add(telebot.types.InlineKeyboardButton('start', callback_data='yes'),
             telebot.types.InlineKeyboardButton('stop', callback_data='no'))

# keyboard buttons instruction
@bot.message_handler(commands=['notifyme'])
def like(message):
  cid = message.chat.id
  if cid in user_dict: 
    bot.send_message(cid, "To start getting new post notifications, press start. To stop notifications, press stop " + u'\U0001F447', reply_markup=keyboard)
  else: 
    bot.send_message(cid, "Please enter the seach URL first " + u'\U0001F605')

# keyboard buttons response - start or stop search
@bot.callback_query_handler(func=lambda call: call.data in ['yes', 'no'])
def callback_hander(call):
    global user_dict 
    chat_id = call.message.chat.id
    if call.data == 'yes':
        # bot.send_message(chat_id, "I got ur id :) " + str(chat_id))
        if chat_id in user_dict: # ----------- subscribe user -----------------
            if user_dict[chat_id][2]: 
                bot.send_message(chat_id, "You're already subscribed :/")
                return 
            user_dict[chat_id][2] = True 
            bot.send_message(chat_id, "You are now subscribed to updates!")

            with concurrent.futures.ThreadPoolExecutor() as executor: 
                executor.submit(send_continuous_updates, chat_id)
        else: 
            bot.send_message(chat_id, "Please enter your Ebay-Kleinanzeigen search URL first " + u'\U0001F613')
    else:
        if chat_id in user_dict: # --------------- unsubscribe ---------------------
            user_dict[chat_id][2] = False
            bot.send_message(chat_id, "You will no longer receive notifications. Goodbye " + u'\U0001F44B')
        else: 
            bot.send_message(chat_id, "No notifications to stop, you never signed up for any " + u'\U0001F9D0')


@bot.message_handler(commands=['help'])
def answer(message):
    bot.reply_to(message, "To get started, enter your search query on Ebay-Kleinanzeigen, copy the URL and send it to me. Then say /notifyme, press start, and you'll get notifications on new posts "+ u'\U0001F48C' + " You can press stop to unsubscribe from notifications. Say clear in your message to delete all your notifications and remove your data. ")

@bot.message_handler(commands=['start'])
def answer(message):
    bot.reply_to(message, "Welcome" + u"\U0001F638" + " My name is Scrapy, I scrape Ebay-Kleinanzeigen to keep you updated on new listings for your search. For more info, say /help.")
    
@bot.message_handler(commands=['clear'])
def answer(message):
    global user_dict 
    chat_id = message.chat.id 
    if chat_id in user_dict: # ---------------- remove user ----------------------
        del user_dict[chat_id] 
        bot.send_message(chat_id, "Your data has been removed. Goodbye " + u'\U0001F44B')
    else: 
        bot.send_message(chat_id, "No notifications to stop, you're not in the database " + u'\U0001F9D0')

@bot.message_handler(func=lambda m: True)
def echo_all(message):
    global url
    greet = '^h.*[iyo]$'
    url_pattern = '^(https://|www.).*'
    if re.match(url_pattern, message.text.lower()):
        if 'https://www.ebay-kleinanzeigen.de/s' or 'https://m.ebay-kleinanzeigen.de/s' in message.text: 
            chat_id = message.chat.id
            # --------------------------- add user ---------------------------------
            user_dict[chat_id] = [message.text, 0, False] # [url, last_checked, subscribed]
            bot.reply_to(message, "Well done, you set up the URL " + u'\U0001F601' + " to get notifications, say /notifyme and press the start button") 
        else:
            bot.reply_to(message, "Please search on Ebay-Kleinanzeigen, then give the full URL. It should take the form of https://www.ebay-kleinanzeigen.de/s-...")
    elif re.match(greet,message.text.lower()):
        bot.reply_to(message, "Hello " + message.chat.first_name + "! Nice to meet you " +  u'\U0001F600')
    elif 'clear' in message.text.lower():
        bot.reply_to(message, "Too bad it didn't work out between us :( ")
    else:
        bot.reply_to(message, "For more info, say /help. To start getting notifications, enter search URL and say /notifyme (press stop to unsubscribe from notifications). " + u'\U0001F4AB') 


def send_updates(chat_id):
    global user_dict
    url, last_checked, subscribed = user_dict[chat_id]

    if not subscribed:
        return

    adid = cost = title = link = date = [] 
    
    print('checking for user ', chat_id)
    try:
        adid, cost, title, link, date = scraper.parse_search(url)
    except Exception as e: 
        print('Error: ', e)
        time.sleep(5)
    print(len(adid), len(cost), len(title), len(link))

    if  len(adid) > 1 and len(adid) ==len(link): 
        zip_postinfo = sorted(zip(adid, cost, title, link))
        # show a couple links for first time user and save last_checked post id 
        if last_checked == 0: 
            bot.send_message(chat_id, "Here's the most recent post " + u'\U0001F48C' + " You'll get notifications for all new posts hereafter.")
            a, c, t, l = zip_postinfo[-1]
            bot.send_message(chat_id, c + " " + t + "\n" + base_url + l)
            user_dict[chat_id][1] = int(a) # set last_checked
        if last_checked >= int(zip_postinfo[-1][0]): 
            # no new posts, break out of function
            return
        for item in zip_postinfo: 
            a, c, t, l = item 
            # print('last checked: ', last_checked, ' new: ', str(a), ' bool ', int(a) > last_checked)
            if last_checked and int(a) > last_checked: 
                print("sent to ", chat_id)
                bot.send_message(chat_id ,"I found something " + u'\U0001F929' + "\n" + c + " " + t + "\n" + base_url + l)
                user_dict[chat_id][1] = int(a) # update last_checked

def send_continuous_updates(chat_id):
    global user_dict 
    while user_dict[chat_id][2]:
        # no notifications between midnight and 5:30am 
        if dt.today().hour < 5:
            time.sleep(30*60) # wait 30min 
            continue 
        send_updates(chat_id) 
        time.sleep(90)

# listener (try this since handler doesn't always work)
def check_unsubscribe(messages):
     global user_dict 
     for message in messages:
         print(message.text.lower())
         if "clear" in message.text.lower():
             chat_id = message.chat.id 
             if chat_id in user_dict: # ---------------- remove user ----------------------
                 del user_dict[chat_id] 
                 bot.send_message(chat_id, "Your data has been removed. Goodbye " + u'\U0001F44B')
             else: 
                 bot.send_message(chat_id, "No notifications to stop, you're not in the database " + u'\U0001F9D0')

bot.set_update_listener(check_unsubscribe)

bot.polling(none_stop=True, interval=1, timeout=5)
